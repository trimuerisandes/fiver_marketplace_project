package com.example.fiverr_project_3fr81.activities;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.fiverr_project_3fr81.R;
import com.google.android.gms.samples.vision.barcodereader.BarcodeCapture;
import com.google.android.gms.samples.vision.barcodereader.BarcodeGraphic;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.barcode.Barcode;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import xyz.belvi.mobilevisionbarcodescanner.BarcodeRetriever;

public class CameraActivity extends AppCompatActivity implements BarcodeRetriever {

    private static final String TAG = "InstantOrderActivity";
    ObjectAnimator mAnimator;
    View mScannerLayout;
    View mScannerBar;
    @BindView(R.id.btnFlashSwitch)
    ImageView mFlash;


    BarcodeCapture mBarcodeCapture;

    boolean mIsFlashOn = false;

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.camera_frame);
        setContentView(R.layout.camera_frame);
        ButterKnife.bind(this);
        mScannerLayout = findViewById(R.id.focus);
        mScannerBar = findViewById(R.id.scannerBar);



        mBarcodeCapture = (BarcodeCapture) getSupportFragmentManager().findFragmentById(R.id.barcode);
        mBarcodeCapture.setBarcodeFormat(Barcode.QR_CODE)
                .setCameraFacing(CameraSource.CAMERA_FACING_BACK);
        mBarcodeCapture.setRetrieval(this);

        mFlash.setOnClickListener(v -> {
            mBarcodeCapture.setShowFlash(!mIsFlashOn);
            mFlash.setImageResource(mIsFlashOn ? R.drawable.ic_flash_off_grey_400_24dp
                    : R.drawable.ic_flash_on_amber_300_24dp);
            mBarcodeCapture.refresh(true);
            mIsFlashOn = !mIsFlashOn;
        });

        mAnimator = null;

        ViewTreeObserver vto = mScannerLayout.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                mScannerLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                float destination = (float) mScannerLayout.getHeight();

                mAnimator = ObjectAnimator.ofFloat(mScannerBar, "translationY",
                        0,
                        destination - (20));

                mAnimator.setRepeatMode(ValueAnimator.REVERSE);
                mAnimator.setRepeatCount(ValueAnimator.INFINITE);
                mAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
                mAnimator.setDuration(3000);
                mAnimator.start();

            }
        });

    }


    @Override
    public void onRetrieved(Barcode barcode) {

    }

    @Override
    public void onRetrievedMultiple(final Barcode closetToClick, final List<BarcodeGraphic> barcodeGraphics) {

    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {
        for (int i = 0; i < sparseArray.size(); i++) {
            Barcode barcode = sparseArray.valueAt(i);
            Log.e("value", barcode.displayValue);
        }

    }

    @Override
    public void onRetrievedFailed(String reason) {

    }

    @Override
    public void onPermissionRequestDenied() {

    }


}
